package com.vinicius.alone.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vinicius.alone.mensagem.Mensagem;
import com.vinicius.alone.model.Usuario;
import com.vinicius.alone.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	Mensagem mensagem;
	
	
	
	
	public List<Usuario> obterUsuarios(){
		List<Usuario> usuarios = (List<Usuario>) usuarioRepository.obterUsuariosAtivos();
		return usuarios;
	}
	
	public Usuario cadastrar(Usuario user) throws ParseException {
		user.setSituacao(Boolean.TRUE);
		user.setDataInclusao(new Date());
		return usuarioRepository.save(user);
		
	}
	
	public Usuario excluir(Long id) throws Exception {
		Usuario user = usuarioRepository.obterUsuarioPorId(id);
		if(user !=null && user.getSituacao()) {
			user.setSituacao(Boolean.FALSE);
			user.setDataExclusao(new Date());
		}else {
			throw new Exception(mensagem.get("me1"));
		}
		return usuarioRepository.save(user);
		
	}
	
	public Usuario ativar(Long id) throws Exception {
		Usuario user = usuarioRepository.obterUsuarioPorId(id);
		if(user !=null && !user.getSituacao()) {
			user.setSituacao(Boolean.TRUE);
			user.setDataInclusao(new Date());
		}else {
			throw new Exception(mensagem.get("me2"));
		}
		return usuarioRepository.save(user);
		
	}
	

}
