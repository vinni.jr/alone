package com.vinicius.alone.mensagem;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component
public class Mensagem {
	
	@Autowired
	private MessageSource messageSource;
	private MessageSourceAccessor accessor;
	
	Locale ptBr = new Locale("pt", "BR");

	@PostConstruct
	private void init() {
		//accessor = new MessageSourceAccessor(messageSource, ptBr);
		accessor = new MessageSourceAccessor(messageSource, Locale.ENGLISH);
	}

	public String get(String code) {
		return accessor.getMessage(code);
	}

}
