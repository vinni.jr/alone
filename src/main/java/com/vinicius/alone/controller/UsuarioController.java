package com.vinicius.alone.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vinicius.alone.model.Usuario;
import com.vinicius.alone.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "*")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<?>> listarUsuariosAtivos() {
		List<Usuario> usuarios = (List<Usuario>) usuarioService.obterUsuarios();
		return new ResponseEntity<List<?>>(usuarios, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/cadastrar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> cadastrarUsuario(@RequestBody Usuario usuario) throws ParseException {
		Usuario usuarioCadastrado = usuarioService.cadastrar(usuario);
		return new ResponseEntity<Usuario>(usuarioCadastrado, HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/excluir/{id}", produces  =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> excluirUsuario(@PathVariable Long id)  throws Exception {
		Usuario usuarioCadastrado = usuarioService.excluir(id);
		return new ResponseEntity<Usuario>(usuarioCadastrado, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/ativar/{id}", produces  =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> ativarUsuario(@PathVariable Long id)  throws Exception {
		Usuario usuarioCadastrado = usuarioService.ativar(id);
		return new ResponseEntity<Usuario>(usuarioCadastrado, HttpStatus.OK);
	}

}
