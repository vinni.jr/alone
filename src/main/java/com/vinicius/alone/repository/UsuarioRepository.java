package com.vinicius.alone.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vinicius.alone.model.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	
	@Query("SELECT USU FROM Usuario USU WHERE USU.situacao = true ")
	public Iterable<Usuario> obterUsuariosAtivos();
	
	@Query("SELECT USU FROM Usuario USU WHERE USU.idUsuario = :idUsuario ")
	public Usuario obterUsuarioPorId(@Param("idUsuario") Long idUsuario);

}
